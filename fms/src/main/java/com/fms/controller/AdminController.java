package com.fms.controller;


import org.springframework.web.bind.annotation.*;

import com.fms.model.Student;
import com.fms.service.AdminService;

import org.springframework.beans.factory.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	 private AdminService adminService;
	 
	@CrossOrigin
	@GetMapping("/allStudents")
	public List<Student> getAllStudents(){	
		return adminService.allStudents();
	}
	@CrossOrigin
	@PutMapping("/updateResult")
	 public Student updateResult(@RequestBody Student student) {
		 adminService.updateStudent(student);
		   return student;
	 }
	
}
