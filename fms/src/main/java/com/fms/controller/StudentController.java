package com.fms.controller;

import org.springframework.web.bind.annotation.*;

import com.fms.model.Student;
import com.fms.model.User;
import com.fms.service.StudentService;

import org.springframework.beans.factory.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api")
public class StudentController {

	
	@Autowired
	private StudentService studentService;
	
	@CrossOrigin
	@GetMapping("/allStudents")
	public List<Student> get(){
		
		return studentService.get();
		
	}
	
	@CrossOrigin
	@PostMapping("/register")
	public Student register(@RequestBody Student student) {
		studentService.registerStudent(student);
		return student;
		
	}
	
	
	@GetMapping("/getStudentById/{id}")
	public Student getUser(@PathVariable int id) {
		  return studentService.get(id);
		
	}
	
	@DeleteMapping("/deletestudentById/{id}")
	public void deleteUser(@PathVariable int id) {
		System.out.println("Inside delete user");
		 studentService.deleteStudent(id);
		
	}
	
	@CrossOrigin
	@PostMapping("/login")
	public String loginUser(@RequestBody Student student) {
		System.out.println("inside the login user!!");
		return studentService.login(student);
	}
	
}
