package com.fms.service;

import java.util.List;

import com.fms.model.Student;

public interface AdminService {

	 List<Student> allStudents();
	 void updateStudent(Student student);
	
}
