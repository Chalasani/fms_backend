package com.fms.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.springframework.stereotype.*;

import com.fms.model.Student;

import org.hibernate.query.*;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.*;


@Repository
public class StudentDAOImpl implements StudentDAO {
	
	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Student> get() {
		// TODO Auto-generated method stub
		Session currentSession=entityManager.unwrap(Session.class);
		String s="from Student";
		
		Query<Student> query =currentSession.createQuery(s,Student.class);
		
	 List<Student> students= query.getResultList();
		
		return students;
	}

	@Override
	public Student get(int id) {
		
		Session currentSession=entityManager.unwrap(Session.class);
		  Student student= currentSession.get(Student.class, id);
		  System.out.println("student"+student);
		  return student;
	}

	@Override
	public void registerStudent(Student student) {
		Session currentSession=entityManager.unwrap(Session.class);
		System.out.println("student"+student);
		   currentSession.save(student);
		
	}

	@Override
	public void deleteStudent(int id) {
		
		Session currentSession=entityManager.unwrap(Session.class);
		Student student= currentSession.get(Student.class, id);
		  System.out.println("user"+student.getId());
		  currentSession.delete(student);
		
	}
	@Override
	public String login(Student student) {
	Session currentSession=entityManager.unwrap(Session.class);
//		  User userdata= currentSession.get(User.class, user.getUsername());
//		  System.out.println("user"+userdata);
		  //return user;
	System.out.println("password"+ student.getPassword());
	System.out.println("user"+student);
	Student u1 = null;
	try {
		u1=(Student)currentSession.createQuery("from Student where username = :username").setParameter("username",student.getUsername()).getSingleResult();
	}
	catch(NoResultException nre) {
		
	}
		 if(u1 == null) {
			 return "0"; // user not found!
		 }
		 if(u1.getPassword().equals(student.getPassword()))
		 {
			 return "1"; // login success;
		 }
		 else
		 {
			 return "2"; // password incorrect
		 }
		
		
	}

	}

