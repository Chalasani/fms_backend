package com.fms.dao;

import java.util.List;

import com.fms.model.Product;
import com.fms.model.Student;


public interface AdminDAO {
	 List<Student> allStudents();
	 void updateStudent(Student student);
}
