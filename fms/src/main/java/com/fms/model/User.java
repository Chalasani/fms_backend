package com.fms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name= "users")
public class User {

	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	@Column
	private Integer id;
	@Column
	private String username;
	@Column
	private String password;
	@Column
	private String emailid;
	@Column
	private String role;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmailIid() {
		return emailid;
	}
	public void setEmailIid(String emailIid) {
		this.emailid = emailIid;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.getEmailIid()+ "\t"+ this.getUsername()+"\t"+ this.getPassword();
	}
	
}
